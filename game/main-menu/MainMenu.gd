extends Node2D

signal new_game_requested
signal exit_program_requested

func _ready():
	var size = get_viewport_rect().size
	$Container.set_size(size)
	$Background.set_size(size)
	activate("", "New Game")

func activate(message, startButtonText):
	visible = true
	setMessage(message)
	setStartButtonText(startButtonText)

	
func setStartButtonText(text):
	$Container/NewGame.text = text

func setMessage(message):
	$Message.text = message
	
func _on_NewGame_pressed():
	emit_signal("new_game_requested")


func _on_Exit_pressed():
	emit_signal("exit_program_requested")
