const level_data = {
	"world_object_templates":
	[
		{
			"type": "resource",
			"id": "resource_1",
			"tile_id": 2,
			"count": 20,
			"name": "Resource 1",
			"coords": [1, 2],
			"size": [3, 2]
		},
		{
			"type": "resource",
			"id": "resource_2",
			"tile_id": 3,
			"count": 50,
			"name": "Resource 2",
			"coords": [7, 12],
			"size": [2, 1]
		},
		{
			"type": "resource",
			"id": "resource_3",
			"tile_id": 5,
			"count": 50,
			"name": "Resource 3",
			"coords": [8, 1],
			"size": [2, 2]
		},
		{
			"type": "resource_storage",
			"id": "resource_storage_1",
			"tile_id": 8,
			"coords": [9, 7],
			"size": [2, 2]
		},
		{
			"type": "teleporter",
			"id": "teleporter_1",
			"tile_id": 7,
			"parts": ["teleporter_part"],
			"build_time": 10,
			"coords": [13, 12],
			"size": [2, 2]
		},
		{
			"type": "gathering_droid",
			"id": "gathering_droid_1",
			"name": "Fred",
			"comment": "Resource count gathered per second",
			"gather_rate": 2,
			"movement_speed": 150,
			"active": true,
			"position": [370, 160],
			"color": [1, 0, 0],
			"texture": "res://assets/gathering-droid-01.png"
		},
		{
			"type": "gathering_droid",
			"id": "gathering_droid_2",
			"name": "Bob",
			"comment": "Resource count gathered per second",
			"gather_rate": 2,
			"movement_speed": 150,
			"active": true,
			"position": [370, 180],
			"color": [1, 1, 0],
			"texture": "res://assets/gathering-droid-02.png"
		},
		{
			"type": "engineering_droid",
			"id": "engineering_droid_1",
			"name": "Joe",
			"movement_speed": 100,
			"active": false,
			"color": [0, 0, 1],
			"texture": "res://assets/engineering-droid-01.png"
		},
		{
			"type": "enemy_droid",
			"name": "Trixie",
			"movement_speed": 100,
			"active": false,
			"texture": "res://assets/enemy-droid-01.png"
		},
		{
			"type": "turret_droid",
			"id": "turret_droid_1",
			"name": "Turret",
			"movement_speed": 200,
			"active": false,
			"position": [200, 180],
			"parts": ["turret_droid_part"],
			"texture": "res://assets/turret-droid-01.png"
		},
		{"type": "teleporter_part", "id": "teleporter_part_1"},
		{"type": "turret_droid_part", "id": "turret_droid_part_1"},
		{
			"type": "printer",
			"id": "printer",
			"name": "PT",
			"tile_id": 6,
			"coords": [12, 8],
			"size": [1, 1],
			"output":
			{
				"engineering_droid":
				{
					"requirements": [{"type": "resource", "id": "resource_1", "count": 5}]
				},
				"teleporter_part":
				{
					"type": "teleporter_part",
					"requirements": [{"type": "resource", "id": "resource_2", "count": 10}]
				},
				"turret_droid_part":
				{
					"requirements": [{"type": "resource", "id": "resource_3", "count": 5}]
				}
			}
		},
	]
}
