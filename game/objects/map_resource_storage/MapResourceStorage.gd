extends "res://game/objects/MapBase.gd"

var resources = {}
signal resource_storage_changed


func init(_template):
	pass


# Unlimited storage for now
func put(amount, resource):
	if not resources.has(resource):
		resources[resource] = 0
	resources[resource] += amount
	Log.debug("ResourceStorage.put: %s %s", [resource, amount])
	emit_signal("resource_storage_changed", amount, resource)


func take(amount, resource):
	if check(resource) < amount:
		return false
	
	Log.debug("ResourceStorage.take: %s", amount)
	resources[resource] -= amount
	return true

func check(resource):
	if not resources.has(resource):
		return 0
	return resources[resource]
