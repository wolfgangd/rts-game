extends Reference

var gathering_droid_scene = load("res://game/objects/gathering_droid/GatheringDroid.tscn")

var engineering_droid_scene = load("res://game/objects/engineering_droid/EngineeringDroid.tscn")

var enemy_droid_scene = load("res://game/objects/enemy_droid/EnemyDroid.tscn")

var turret_droid_scene = load("res://game/objects/turret_droid/TurretDroid.tscn")

var resource_object_scene = load("res://game/objects/map_resource/MapResource.tscn")

var resource_storage_scene = load("res://game/objects/map_resource_storage/MapResourceStorage.tscn")

var printer_object_scene = load("res://game/objects/map_printer/MapPrinter.tscn")

var teleporter_object_scene = load("res://game/objects/map_teleporter/MapTeleporter.tscn")

var building_material_scene = load("res://game/objects/building_material/BuildingMaterial.tscn")

var object_scenes = {
	"gathering_droid": gathering_droid_scene,
	"engineering_droid": engineering_droid_scene,
	"enemy_droid": enemy_droid_scene,
	"turret_droid": turret_droid_scene,
	"resource": resource_object_scene,
	"resource_storage": resource_storage_scene,
	"printer": printer_object_scene
}

signal droid_created
signal resource_created
signal resource_storage_created
signal printer_created
signal teleporter_created
signal teleporter_part_created

var tile_map

var __current_id


func _init(tile_map_ = null):
	tile_map = tile_map_
	__current_id = 1


func create(template, arg = null):
	match template["type"]:
		"gathering_droid", "engineering_droid", "enemy_droid", "turret_droid":
			return __create_droid(template, arg)
		"resource":
			return __create_map_object(template)
		"resource_storage":
			return __create_map_object(template)
		"printer":
			return __create_map_object(template, arg)
		"teleporter":
			return __create_teleporter(template, arg)

	assert(false, "Invalid template type: " + template["type"])


func __create_droid(template, position = null):
	var type = template["type"]
	assert(object_scenes.has(type), "Invalid droid type: " + type)
	var droid = object_scenes[type].instance()
	if position == null:
		position = template["position"]
	droid.set_position(Vector2(position[0], position[1]))
	droid.init(template)
	__identify(droid, template)
	emit_signal("droid_created", droid)
	return droid


func __create_teleporter(template, coords):
	var teleporter = teleporter_object_scene.instance()
	teleporter.place_on_map(template, tile_map, coords)
	teleporter.init(template)
	__identify(teleporter, template)
	emit_signal("teleporter_created", teleporter)
	return teleporter


func __create_map_object(template, arg = null):
	var type = template["type"]
	assert(object_scenes.has(type), "Invalid object type: " + type)

	# TODO: init is dependent of place_on_map:
	# Resources counts display different if order reversed

	var obj = object_scenes[type].instance()
	obj.place_on_map(template, tile_map)
	if arg != null:
		obj.init(template, arg)
	else:
		obj.init(template)

	__identify(obj, template)

	emit_signal(type + "_created", obj)
	return obj


func __identify(obj, template):
	obj.set_type(template["type"])
	obj.set_id("%s_%d" % [obj.get_type(), __current_id])
	__current_id += 1
