extends "res://game/objects/MapBase.gd"

signal teleporter_ready;

enum { UNDER_CONSTRUCTION, READY }

var build_time
var elapsed_build_time = 0.0
var construction_progress = 0
var timer
var state


func init(template):
	build_time = template["build_time"]
	$MapObject/Info.set_size($MapObject.rect.size)
	$MapObject/Info.text = "0 %"


func _enter_tree():
	pass
	#timer = Timer.new()
	#add_child(timer)
	#timer.connect("timeout", self, "_on_construction_progress")
	#timer.start(1.0)


func start_building():
	state = UNDER_CONSTRUCTION
	timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "_on_construction_progress")
	timer.start(1.0)


func _on_construction_progress():
	elapsed_build_time += 1.0
	var progress = elapsed_build_time / float(build_time)
	if progress >= 1.0:
		state = READY
		timer.stop()
		$MapObject/Info.text = ""
		emit_signal("teleporter_ready")
	else:
		$MapObject/Info.text = "%.0f %%" % (progress * 100)
