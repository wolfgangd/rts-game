extends "res://game/objects/Droid.gd"

enum { STATE_IDLE, STATE_GO_SOMEWHERE }

var __level_objects
var __timer
var __scan_radius

var pulsing_rect_effect = load("res://game/effects/pulsing_rect/PulsingRect.tscn")
var fading_line_effect = load("res://game/effects/fading_line/FadingLine.tscn")


func on_ready():
	state = STATE_IDLE
	__scan_radius = 200
	__timer = Timer.new()
	add_child(__timer)
	__timer.connect("timeout", self, "_on_scan_timeout")
	__timer.start(3.0)


func _on_destination_reached():
	Log.debug("TurretDroid destination reached, state = %s", state)
	state = STATE_IDLE


func _on_scan_timeout():
	var enemy_droids = __level_objects.get_objects_with_type("enemy_droid")
	var my_position = get_position()
	for enemy_droid in enemy_droids:
		var distance = my_position.distance_to(enemy_droid.get_position())
		if distance <= __scan_radius:
			Log.debug("distance to enemy droid: %f", distance)
			Log.game("Turret Droid %s zaps Enemy Droid %s", [get_id(), enemy_droid.get_id()])
			self.__play_destruction_effects(enemy_droid)
			__level_objects.destroy_object(enemy_droid)
			break
		
func set_level_objects(level_objects):
	__level_objects = level_objects


func __play_destruction_effects(enemy_droid):
	var effect1 = pulsing_rect_effect.instance()
	effect1.position = enemy_droid.get_position()
	add_child(effect1)
	
	var effect2 = fading_line_effect.instance()
	add_child(effect2)
	effect2.init(enemy_droid.get_position() + Vector2(8, 8), get_position() + Vector2(8, 8), Color.yellow)
