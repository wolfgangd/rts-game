extends "res://game/objects/Droid.gd"

enum {
	STATE_IDLE,
	STATE_WAIT_FOR_RESOURCE,
	STATE_GOTO_RESOURCE,
	STATE_COLLECT,
	STATE_COLLECT_DONE,
	STATE_UNLOAD
}

var resource
var storage
var holding_count


func on_ready():
	state = STATE_IDLE
	holding_count = 0
	commands.append({"id": "collect"})


func is_idle():
	return state == STATE_IDLE


func _on_command(command):
	if state == STATE_IDLE:
		match command["id"]:
			"collect":
				state = STATE_WAIT_FOR_RESOURCE


func collect_resource(resource_):
	if state == STATE_WAIT_FOR_RESOURCE:
		resource = resource_
		goto_resource()
		return true
	return false


func goto_resource():
	state = STATE_GOTO_RESOURCE
	$Mover.move_close_to(resource.get_rect())


func _on_destination_reached():
	if state == STATE_GOTO_RESOURCE:
		state = STATE_COLLECT
		var timer = get_tree().create_timer(2.0)
		timer.connect("timeout", self, "_on_collect_done")
	if state == STATE_COLLECT_DONE:
		state = STATE_UNLOAD
		var timer = get_tree().create_timer(1.0)
		timer.connect("timeout", self, "_on_unload_done")


func _on_collect_done():
	if state == STATE_COLLECT:
		state = STATE_COLLECT_DONE
		holding_count = resource.take(10)
		if holding_count > 0:
			$Mover.move_close_to(storage.get_rect())
		else:
			state = STATE_IDLE


func _on_unload_done():
	if state == STATE_UNLOAD:
		storage.put(holding_count, resource.resource_id)
		goto_resource()
