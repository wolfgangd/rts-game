extends "res://game/objects/ObjectBase.gd"


func set_size(size):
	$ColorRect.set_size(size)


func set_color(color):
	$ColorRect.color = color
