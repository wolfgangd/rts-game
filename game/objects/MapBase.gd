extends "res://game/objects/ObjectBase.gd"


func place_on_map(definition, tile_map, coords_override = null):
	$MapObject.place_on_map(definition, tile_map, coords_override)


func on_clicked(receiver, fn):
	return $MapObject.on_clicked(receiver, fn, self)


func get_rect():
	return $MapObject.rect


func get_name():
	return $MapObject.object_name


func get_position():
	return $MapObject.position
