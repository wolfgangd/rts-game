
extends "res://game/objects/Droid.gd"

signal placing_required

enum { STATE_IDLE, STATE_GOTO_PRINTER, STATE_GOTO_BUILD_SITE }

var carried_part
var __printer
var __level_template
var __object_factory
var __build_coords
var __template_to_build

var pickup_time = 1.0


func on_ready():
	state = STATE_IDLE
	commands.append({"id": "build-teleporter"})
	commands.append({"id": "build-turret-droid"})


func set_object_factory(object_factory):
	__object_factory = object_factory


func set_level_template(level_template):
	__level_template = level_template


func is_idle():
	return state == STATE_IDLE


func _on_command(command):
	if state == STATE_IDLE:
		match command["id"]:
			"build-teleporter":
				__template_to_build = __level_template.get_first_template_with_type("teleporter")
				Log.debug("EngineerinDroid -> build teleporter %s", [__template_to_build])
				emit_signal("placing_required", __template_to_build)

			"build-turret-droid":
				__template_to_build = __level_template.get_first_template_with_type("turret_droid")
				Log.debug("EngineerinDroid -> build turret droid %s", [__template_to_build])
				emit_signal("placing_required", __template_to_build)


func goto_printer(printer, build_coords):
	Log.debug("goto_printer %s", [build_coords])
	__printer = printer
	__build_coords = build_coords
	state = STATE_GOTO_PRINTER
	navigate_to(__printer.get_position())


func _on_destination_reached():
	Log.debug("EngineeringDroid destination reached, state = %s", state)
	if state == STATE_GOTO_PRINTER:

		assert(__template_to_build.has("parts"), "Invalid parts specification in %s" % __template_to_build)
		var parts = __template_to_build["parts"]
		
		for part in parts:
			carried_part = __printer.take_part(part)
			if carried_part == null:
				Log.game("Missing part %s for building turret droid", [part])
				state = STATE_IDLE
				return
		state = STATE_IDLE
		var timer = get_tree().create_timer(pickup_time)
		timer.connect("timeout", self, "_on_part_picked_up")
	if state == STATE_GOTO_BUILD_SITE:
		__object_factory.create(__template_to_build, [__build_coords.x, __build_coords.y])
		state = STATE_IDLE


func _on_part_picked_up():
	Log.debug("_on_part_pickup")
	state = STATE_GOTO_BUILD_SITE
	navigate_to(__build_coords - Vector2(16, 16))
