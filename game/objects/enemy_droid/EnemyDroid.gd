extends "res://game/objects/Droid.gd"

enum { STATE_IDLE, STATE_GO_SOMEWHERE, STATE_DRAIN_RESOURCE, STATE_DEAD }

var resource
var timer


func _exit_tree():
	state = STATE_DEAD
	timer.stop()


func on_ready():
	timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "_on_timeout")

	state = STATE_GO_SOMEWHERE
	Log.debug("Enemy Droid moves to resource")
	move_close_to(resource.get_rect())


func set_resource(resource_):
	resource = resource_


func _on_timeout():
	match state:
		STATE_DRAIN_RESOURCE:
			var time_to_next = randi()%5 + 1
			Log.game("Enemy Droid %s drains resource %s [to next: %d]", [get_id(), resource.get_id(), time_to_next])
			resource.take(1)
			timer.set_wait_time(time_to_next)

func _on_destination_reached():
	Log.debug("EnemyDroid reached resource, state = %s ", state)
	if state == STATE_GO_SOMEWHERE:
		state = STATE_DRAIN_RESOURCE
		timer.set_wait_time(randi()%5 + 1)
		timer.start()

