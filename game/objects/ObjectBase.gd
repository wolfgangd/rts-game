extends Node

var __type
var __id


func get_type():
	return __type


func get_id():
	return __id


func set_type(type_):
	__type = type_


func set_id(id_):
	__id = id_


func get_name():
	assert(false, "get_name not implemented")


func get_position():
	assert(false, "get_position not implemented")
