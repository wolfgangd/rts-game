extends "res://game/objects/MapBase.gd"

var count
var resource_id


func init(template):
	count = template["count"]
	resource_id = template["id"]
	if $MapObject.rect:
		$MapObject/Info.set_size($MapObject.rect.size)
	$MapObject/Info.text = str(count)


func take(amount):
	var taken = min(amount, count)
	count -= taken
	$MapObject/Info.text = str(count)
	return taken
