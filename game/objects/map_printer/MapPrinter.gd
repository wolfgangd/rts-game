extends "res://game/objects/MapBase.gd"

var output
var commands = []

var level_template
var object_factory
var __resource_storage

var __part

var __printed_parts = {}


func init(template, storage):
	output = template["output"].duplicate(true)
	__resource_storage = storage


func setup(level_template_, object_factory_):
	level_template = level_template_
	object_factory = object_factory_


func _on_command(command):
	Log.debug("Log.debuger on_command %s", command)
	match command["id"]:
		"print":
			var template_type = command["arg"]
			var template = level_template.get_first_template_with_type(template_type)
			assert(template != null, "Template with with type '%s' was not found" % template_type)
			if !__use_resources_for(template_type):
				Log.game("Not enough resources to print %s", [template_type])
				return
			match template_type:
				"engineering_droid":
					Log.debug("print engineering droid")
					object_factory.create(template, get_rect().end)
				"turret_droid_part":
					Log.debug("print turret droid")
					self.__add_part({}, "turret_droid_part")
				"teleporter_part":
					Log.debug("print teleporter part")
					self.__add_part({}, "teleporter_part")


func get_commands():
	var result = commands.duplicate()
	for key in output:
		result.append({"id": "print", "arg": key, "enabled": true})
	return result


func take_part(type):
	if !self.__printed_parts.has(type) || self.__printed_parts[type].size() == 0:
		return null
	return self.__printed_parts[type].pop_back()


func __add_part(part, type):
	if !self.__printed_parts.has(type):
		self.__printed_parts[type] = []
	self.__printed_parts[type].append(part)


func __use_resources_for(template_type):
	var op = output[template_type]
	for r in op["requirements"]:
		if __resource_storage.check(r["id"]) < r["count"]:
			return false

	for r in op["requirements"]:
		__resource_storage.take(r["count"], r["id"])
	return true
