extends "res://game/objects/ObjectBase.gd"

signal state_changed

var droid_name
var droid_color
var is_selected = false
var state
var commands = [{"id": "stop"}]


func _ready():
	on_ready()
	var size = $Mover/TextureRect.get_size()
	if $Mover.has_node("Clickable"):
		$Mover/Clickable.init(size)

	return $Mover.connect("destination_reached", self, "_on_destination_reached")


func on_ready():
	pass


func collect_resource(_resource):
	return false


func init(template):
	droid_name = template["name"]
	if template.has("color"):
		var template_color = template["color"]
		droid_color = Color(template_color[0], template_color[1], template_color[2])
	if template.has("texture"):
		var texture = load(template["texture"])
		$Mover/TextureRect.set_texture(texture)
	if template.has("movement_speed"):
		$Mover.speed = template["movement_speed"]


func get_name():
	return droid_name


func on_clicked(receiver, method):
	return $Mover/Clickable.connect("object_clicked", receiver, method, [self])


func set_selected(b):
	is_selected = b
	$Mover/Clickable.select(b)


func set_state(state_):
	state = state_
	emit_signal("state_changed", state)


func set_position(position):
	$Mover.position = position


func get_position():
	return $Mover.position


func move_close_to(rect):
	$Mover.move_close_to(rect)


func navigate_to(end_position):
	$Mover.navigate_to(end_position)


func get_commands():
	return commands
