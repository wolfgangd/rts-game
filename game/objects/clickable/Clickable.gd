extends Control

signal object_clicked


func init(size):
	set_size(size)
	$HoverIndicator.set_size(size)
	$SelectionIndicator.set_size(size)
	$HoverIndicator.visible = false
	$SelectionIndicator.visible = false

	connect("mouse_entered", self, "_on_mouse_entered")
	connect("mouse_exited", self, "_on_mouse_exited")


func set_border_width(value):
	$HoverIndicator.border_width = value
	$SelectionIndicator.border_width = value


func _on_mouse_entered():
	$HoverIndicator.visible = true


func _on_mouse_exited():
	$HoverIndicator.visible = false


func select(b):
	$SelectionIndicator.visible = b


func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			emit_signal("object_clicked")
