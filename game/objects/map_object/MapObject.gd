extends Node2D

var rect
var object_name


func place_on_map(template, tile_map, coords_override = null):
	object_name = template["name"] if template.has("name") else null
	var size = template["size"]
	var coords;
	if coords_override != null:
		var temp = Vector2(coords_override[0], coords_override[1]) / tile_map.cell_size
		coords=[temp.x as int, temp.y as int]
	else:
		coords = template["coords"]
	var tile_id = template["tile_id"]
	var cell_size = tile_map.cell_size
	position = Vector2(coords[0] * cell_size.x, coords[1] * cell_size.y)
	var size_on_map = Vector2(size[0] * cell_size.x, size[1] * cell_size.y)
	for col in range(size[0]):
		for row in range(size[1]):
			tile_map.set_cell(col + coords[0], row + coords[1], tile_id)

	rect = Rect2(position, size_on_map)
	$Clickable.init(rect.size)
	$Clickable.set_border_width(4)


func on_clicked(receiver, fn, obj):
	return $Clickable.connect("object_clicked", receiver, fn, [obj])
