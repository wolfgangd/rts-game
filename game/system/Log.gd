extends Node

const DEBUG = "DEBUG"
const GAME = "GAME"

var __log_types = {}


func toggleDebug():
	__toggle(DEBUG)


func toggleGame():
	__toggle(GAME)


func debug(message, args = []):
	__log(DEBUG, message, args)


func game(message, args = []):
	__log(GAME, message, args)


func __toggle(type):
	__log_types[type] = true if not __log_types.has(type) else not __log_types[type]


func __log(type, message, args = []):
	if not (__log_types.has(type) and __log_types[type]):
		return
	var __args = args if args is Array else [args]
	var __time_dict = Time.get_datetime_dict_from_system()
	var __time = (
		"%d-%02d-%02d %02d:%02d:%02d"
		% [
			__time_dict["year"],
			__time_dict["month"],
			__time_dict["day"],
			__time_dict["hour"],
			__time_dict["minute"],
			__time_dict["second"]
		]
	)
	print("[%s] %s %s" % [type, __time, message % __args])
