var data


func _init(data_ = {}):
	data = data_


func get_template_by_id(id):
	return __get_first_with_field_value("id", id)


func get_first_template_with_type(type):
	return __get_first_with_field_value("type", type)


func __get_first_with_field_value(field, value):
	for wo in get_all_templates():
		if wo.has(field) and wo[field] == value:
			return wo
	return null


func get_all_templates():
	return data["world_object_templates"]
