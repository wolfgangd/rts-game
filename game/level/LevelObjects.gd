signal object_destroyed

var data = {}


func register_object(obj):
	var type = obj.get_type()
	var id = obj.get_id()
	if not data.has(type):
		data[type] = {}
	data[type][id] = obj


func destroy_object(obj):
	if data.has(obj.get_type()):
		data[obj.get_type()].erase(obj.get_id())
		emit_signal("object_destroyed", obj)


func get_object(type, id):
	if not data.has(type):
		return null
	if not data[type].has(id):
		return null
	return data[type][id]


func get_objects_with_type(type):
	if data.has(type):
		return data[type].values()
	return []
