extends Reference

var map_width
var map_height
var tilemap
var processed_tiles


func _init(map_size):
	map_width = map_size.x
	map_height = map_size.y


func collect(tile_id, cb, tilemap_):
	tilemap = tilemap_
	processed_tiles = {}
	var rectangles = []
	for y in range(map_height):
		for x in range(map_width):
			if not Vector2(x, y) in processed_tiles:
				_add_rectangle_at(x, y, tile_id, rectangles)

	var cell_size = tilemap.cell_size
	for obstacle in rectangles:
		var first_tile = obstacle[0]
		var last_tile = obstacle[obstacle.size() - 1]
		var pos = first_tile * cell_size
		var size = (last_tile - first_tile + Vector2.ONE) * cell_size

		var points = [
			pos, pos + Vector2(size.x, 0), pos + Vector2(size.x, size.y), pos + Vector2(0, size.y)
		]
		cb.call_func(points)


func _add_rectangle_at(x, y, tile_id, rectangles):
	var rectangle = []
	for yy in range(y, map_height):
		if tilemap.get_cell(x, yy) != tile_id:
			break
		for xx in range(x, map_width):
			var cand = Vector2(xx, yy)
			if not processed_tiles.has(cand) and tilemap.get_cell(xx, yy) == tile_id:
				rectangle.append(cand)
				processed_tiles[cand] = true
			else:
				break
	if !rectangle.empty():
		rectangles.append(rectangle)
