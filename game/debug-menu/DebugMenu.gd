extends Node2D

signal debug_win_level
signal debug_lose_level
var __game
var __level_objects

func init(game, level_objects):
	__game = game
	__level_objects = level_objects


	
func _on_Win_Level_pressed():
	__game.win_level()


func _on_Lose_Level_pressed():
	__game.lose_level()

func _on_Stop_enemies_pressed():
	__game.max_enemy_droids = 0

func _on_Fill_Resources_pressed():
	var resources = []
	for resource in __level_objects.get_objects_with_type("resource"):
		resources.append(resource.get_id())
	for storage in __level_objects.get_objects_with_type("resource_storage"):
		for id in resources:
			storage.put(1000, id)
		
