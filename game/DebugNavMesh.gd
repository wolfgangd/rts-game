extends Node2D

var navpoly


func _draw():
	if navpoly == null:
		return
	for id in range(navpoly.get_polygon_count()):
		var polygon = navpoly.get_polygon(id)
		var vertices = []
		for index in polygon:
			vertices.append(navpoly.get_vertices()[index])
		draw_polyline(vertices, Color.red, 1)
		draw_line(vertices[vertices.size() - 1], vertices[0], Color.red, 1)


func set_navpoly(navpoly_):
	navpoly = navpoly_
	update()
