
extends Node2D

export(float) var character_speed = 100.0
var path = []

const map_width = 16
const cell_size = 32
const map_height = 16

var max_enemy_droids = 10
var droid_size = 16
var nav_gen
var num_enemy_droids = 0

var teleporter_parts = []

signal navpoly_ready
signal droid_spawned
signal object_selected
signal printer_spawned
signal level_lost
signal level_won

var gathering_droid_scene = load(
	"res://game/objects/gathering_droid/GatheringDroid.tscn"
)
var building_material_scene = load(
	"res://game/objects/building_material/BuildingMaterial.tscn"
)
var engineering_droid_scene = load(
	"res://game/objects/engineering_droid/EngineeringDroid.tscn"
)
var map_object_scene = load("res://game/objects/map_object/MapObject.tscn")
var printer_object_scene = load("res://game/objects/map_printer/MapPrinter.tscn")
var resource_object_scene = load("res://game/objects/map_resource/MapResource.tscn")
var teleporter_object_scene = load(
	"res://game/objects/map_teleporter/MapTeleporter.tscn"
)
var resource_storage_scene = load(
	"res://game/objects/map_resource_storage/MapResourceStorage.tscn"
)

const NavGenerator = preload("res://game/navigation/NavGenerator.gd")
const LevelTemplate = preload("res://game/level/LevelTemplate.gd")
const LevelObjects = preload("res://game/level/LevelObjects.gd")
const EngineeringDroid = preload("res://game/objects/engineering_droid/EngineeringDroid.gd")
const ObjectFactory = preload("res://game/objects/ObjectFactory.gd")

var selected_droid
var resource_storage
var teleporter_spawned = false
var tile_map
var level_template
var level_objects
var object_factory
var enemy_spawn_timer
var template_to_place
var overwhelming_numbers = 5

func start(level_data):
	randomize()
	tile_map = $TileMap
	level_template = LevelTemplate.new(level_data)
	level_objects = LevelObjects.new()
	level_objects.connect("object_destroyed", self, "_on_object_destroyed")
	$DebugMenu.init(self, level_objects)
	$UILayer.init(self)
	object_factory = ObjectFactory.new($TileMap)
	object_factory.connect("droid_created", self, "_on_droid_created")
	object_factory.connect("resource_created", self, "_on_resource_created")
	object_factory.connect("resource_storage_created", self, "_on_resource_storage_created")
	object_factory.connect("printer_created", self, "_on_printer_created")
	object_factory.connect("teleporter_created", self, "_on_teleporter_created")
	object_factory.connect("teleporter_part_created", self, "_on_teleporter_part_created")
	spawn_world_object_templates()
	nav_gen = NavGenerator.new(Vector2(map_width, map_height), Vector2(droid_size, droid_size))
	nav_gen.generate($TileMap/NavPoly.navpoly, $TileMap)
	emit_signal("navpoly_ready", $TileMap/NavPoly.navpoly)
	$UILayer.connect("map_selection_clicked", self, "_on_map_selection_clicked")
	$UILayer.connect("map_clicked", self, "_on_map_clicked")
	enemy_spawn_timer = Timer.new()
	add_child(enemy_spawn_timer)
	enemy_spawn_timer.connect("timeout", self, "_on_enemy_spawn_timeout")
	enemy_spawn_timer.start(5)


func _on_enemy_spawn_timeout():
	if num_enemy_droids >= overwhelming_numbers:
		self.lose_level()
	
	if num_enemy_droids >= max_enemy_droids:
		return
	num_enemy_droids += 1
	

	var template = level_template.get_first_template_with_type("enemy_droid")
	var max_pos = 32 * 16 - droid_size
	var rand_pos = randi() % max_pos
	match randi() % 4:
		0:
			object_factory.create(template, [rand_pos, 0])
		1:
			object_factory.create(template, [max_pos, rand_pos])
		2:
			object_factory.create(template, [rand_pos, max_pos])
		3:
			object_factory.create(template, [0, rand_pos])


func _on_object_destroyed(obj):
	remove_child(obj)
	obj.free()
	num_enemy_droids -= 1


func _on_droid_created(droid):
	match droid.get_type():
		"engineering_droid":
			droid.set_level_template(level_template)
			droid.set_object_factory(object_factory)
			droid.on_clicked(self, "_on_droid_clicked")
			droid.connect("placing_required", self, "_on_printer_placing_required")

		"enemy_droid":
			var resources = level_objects.get_objects_with_type("resource")
			droid.set_resource(resources[randi() % resources.size()])
		"turret_droid":
			droid.set_level_objects(level_objects)

	emit_signal("droid_spawned", droid, "_on_droid_clicked")
	level_objects.register_object(droid)
	add_child(droid)


func _on_resource_created(resource):
	resource.on_clicked(self, "_on_resource_clicked")
	level_objects.register_object(resource)
	add_child(resource)


func _on_resource_storage_created(storage):
	level_objects.register_object(storage)
	add_child(storage)


func _on_printer_created(printer):
	printer.on_clicked(self, "_on_printer_clicked")
	emit_signal("printer_spawned", printer, "_on_printer_clicked")
	printer.setup(level_template, object_factory)
	level_objects.register_object(printer)

	add_child(printer)


func _on_teleporter_created(teleporter):
	add_child(teleporter)
	nav_gen.generate($TileMap/NavPoly.navpoly, $TileMap)
	emit_signal("navpoly_ready", $TileMap/NavPoly.navpoly)
	teleporter_spawned = true
	teleporter.connect("teleporter_ready", self, "__on_teleporter_ready")
	teleporter.start_building()

func __on_teleporter_ready():
	win_level()

	
func _on_teleporter_part_created(_part):
	pass


func _on_map_selection_clicked(upper_left_coords):
	$UILayer.hide_map_selection_indicator()
	if template_to_place != null:
		var printer = self.level_objects.get_objects_with_type("printer")[0]

		selected_droid.goto_printer(printer,  upper_left_coords)
		template_to_place = null
		return
	if !teleporter_spawned and selected_droid.get_type() == "engineering_droid":
		var template = level_template.get_first_template_with_type("teleporter")
		object_factory.create(template, [upper_left_coords.x, upper_left_coords.y])


func _on_map_clicked(mouse_position):
	if selected_droid != null:
		selected_droid.set_selected(false)
		selected_droid.navigate_to(mouse_position)
		selected_droid = null


func _on_resource_clicked(resource):
	if selected_droid != null:
		selected_droid.collect_resource(resource)


func _on_droid_clicked(droid):
	var prev_selected = selected_droid
	if selected_droid == droid:
		selected_droid.set_selected(false)
		selected_droid = null
	else:
		if selected_droid != null:
			selected_droid.set_selected(false)
		selected_droid = droid
		selected_droid.set_selected(true)
	if prev_selected != selected_droid:
		emit_signal("object_selected", selected_droid)


func _on_printer_clicked(printer):
	Log.debug("printer clicked: %s", printer.get_id())
	emit_signal("object_selected", printer)


func _on_printer_placing_required(template):
	if template["type"] == "turret_droid":
		$UILayer.show_placement_indicator(Vector2(droid_size, droid_size), false)
		template_to_place = template
	if template["type"] == "teleporter":
		if !teleporter_spawned:
			$UILayer.show_placement_indicator(Vector2(2, 2), true)
			template_to_place = template


func spawn_world_object_templates():
	for template in level_template.get_all_templates():
		match template["type"]:
			"resource":
				object_factory.create(template)
			"resource_storage":
				resource_storage = object_factory.create(template)
			"gathering_droid":
				var droid = object_factory.create(template)
				# TODO Storage could be null depending on order in level data
				droid.storage = resource_storage
			"printer":
				# TODO Storage could be null depending on order in level data
				object_factory.create(template, resource_storage)


func win_level():
	emit_signal("level_won")

func lose_level():
	emit_signal("level_lost")
