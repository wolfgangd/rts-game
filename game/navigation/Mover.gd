extends Node2D

export(float) var speed = 300.0
var path = []
var my_size = 16  # TODO: Calculate from children

signal destination_reached


func _process(delta):
	var walk_distance = speed * delta
	move_along_path(walk_distance)


func move_along_path(distance):
	var last_point = position
	while path.size():
		var distance_between_points = last_point.distance_to(path[0])
		# The position to move to falls between two points.
		if distance <= distance_between_points:
			position = last_point.linear_interpolate(path[0], distance / distance_between_points)
			return
		# The position is past the end of the segment.
		distance -= distance_between_points
		last_point = path[0]
		path.remove(0)
	# The character reached the end of the path.
	position = last_point
	emit_signal("destination_reached")
	set_process(false)


func move_close_to(rect):
	var size = rect.size
	var on_top = rect.position + Vector2(size.x / 2 - my_size / 2, 0)
	var on_bottom = rect.end - Vector2(size.x / 2 + my_size / 2, 0)
	var on_left = rect.position + Vector2(0, size.y / 2 - my_size / 2)
	var on_right = rect.end - Vector2(0, size.y / 2 + my_size / 2)
	choose_shortest_path([on_top, on_bottom, on_left, on_right])


func choose_shortest_path(destinations):
	var best_path = _path_to(destinations[0])
	for i in range(1, destinations.size()):
		var p = _path_to(destinations[i])
		if p.size() < best_path.size():
			best_path = p

	_set_navigation_path(best_path)


func navigate_to(end_position):
	_set_navigation_path(
		Navigation2DServer.map_get_path(
			get_world_2d().get_navigation_map(), position, end_position, true
		)
	)


func _set_navigation_path(path_):
	path = path_
	path.remove(0)
	set_process(true)


func _path_to(new_position):
	return Navigation2DServer.map_get_path(
		get_world_2d().get_navigation_map(), position, new_position, true
	)
