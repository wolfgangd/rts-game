extends Reference

var map_size
var tilemap
var navpoly
var player_size

const RectangleCollector = preload("res://game/tilemap/RectangleCollector.gd")


func _init(map_size_, player_size_):
	map_size = map_size_
	player_size = player_size_


func generate(navpoly_, tilemap_):
	navpoly = navpoly_
	tilemap = tilemap_

	navpoly.clear_outlines()
	var outline_size = map_size * tilemap.cell_size
	navpoly.add_outline(
		[Vector2(0, 0), Vector2(outline_size.x, 0), outline_size, Vector2(0, outline_size.y)]
	)

	var collision_tile_ids = []
	var tile_set = tilemap.tile_set
	for tile_id in tile_set.get_tiles_ids():
		if tile_set.tile_get_shape_count(tile_id):
			collision_tile_ids.append(tile_id)

	var ref = funcref(self, "_add_outline")
	var rect_collector = RectangleCollector.new(map_size)
	for tile_id in collision_tile_ids:
		rect_collector.collect(tile_id, ref, tilemap)

	navpoly.make_polygons_from_outlines()


func _add_outline(points):
	var adjusted_points = [
		points[0] - player_size,
		Vector2(points[1].x, points[1].y - player_size.y),
		points[2],
		Vector2(points[3].x - player_size.x, points[3].y)
	]

	navpoly.add_outline(adjusted_points)
