extends Control

signal clicked


func init():
	$ColorRect.color = Color(0.0, 0.2, 0.0, 0.2)


func set_size(size, keep_margins = false):
	.set_size(size, keep_margins)
	$ColorRect.set_size(size)


func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			emit_signal("clicked")
