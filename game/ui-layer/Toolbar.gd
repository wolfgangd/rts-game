extends HBoxContainer

var game_state


func init(game_state_):
	game_state = game_state_
	game_state.connect("droid_spawned", self, "_on_droid_spawned")
	game_state.connect("printer_spawned", self, "_on_printer_spawned")


func _on_droid_spawned(droid, on_pressed_fn):
	Log.debug("droid spawned :%s", droid)
	var type = droid.get_type()
	if type == "enemy_droid":
		return
	if type == "turret_droid":
		return
	var button = Button.new()
	var normal_style = StyleBoxFlat.new()
	var hover_style = StyleBoxFlat.new()
	button.add_stylebox_override("normal", normal_style)
	button.add_stylebox_override("hover", hover_style)
	button.add_stylebox_override("focus", StyleBoxEmpty.new())
	button.add_stylebox_override("pressed", StyleBoxEmpty.new())
	button.text = droid.get_name()
	button.add_color_override("font_color", Color.black)
	var c = droid.droid_color
	normal_style.bg_color = c
	normal_style.border_width_left = 10
	normal_style.border_width_right = 10
	normal_style.border_color = c
	var hover_bg = Color(c.r, c.g, c.b, 0.7)
	hover_style.bg_color = hover_bg
	add_child(button)
	return button.connect("pressed", game_state, on_pressed_fn, [droid])


func _on_printer_spawned(printer, on_pressed_fn):
	var button = Button.new()
	button.text = printer.get_name()
	add_child(button)
	return button.connect("pressed", game_state, on_pressed_fn, [printer])
