extends VBoxContainer

var game_state


func init(game_state_):
	game_state = game_state_
	game_state.connect("object_selected", self, "_on_object_selected")


func _on_object_selected(object):
	Log.debug("object selected: %s", object)
	for child in get_children():
		remove_child(child)
		child.free()

	if object != null:
		for command in object.get_commands():
			var button = Button.new()
			button.connect("pressed", object, "_on_command", [command])
			button.text = command["id"]
			if command.has("arg"):
				button.text += " " + command["arg"]
			button.disabled = command.has("enabled") and !command["enabled"]

			add_child(button)
