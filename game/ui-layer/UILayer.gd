extends CanvasLayer

signal map_selection_clicked
signal map_clicked

var tile_map
var selected_droid
var mouse_position_divisor


func init(game_state):
	tile_map = game_state.tile_map
	$Toolbar.init(game_state)
	$ToolbarSelection.init(game_state)
	$MapSelectionIndicator.init()
	$MapSelectionIndicator.connect("clicked", self, "_on_map_selection_clicked")
	game_state.connect("navpoly_ready", self, "_on_navpoly_ready")
	hide_map_selection_indicator()


func _unhandled_input(event):
	if !$MapSelectionIndicator.visible:
		return
	if event is InputEventMouseMotion and $MapSelectionIndicator.visible:
		position_map_selection_indicator()
	elif event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		emit_signal("map_clicked", get_viewport().get_mouse_position())


func _on_navpoly_ready(navpoly):
	$DebugNavMesh.set_navpoly(navpoly)


func _on_map_selection_clicked():
	emit_signal("map_selection_clicked", $MapSelectionIndicator.get_position())


func show_placement_indicator(size, snap_to_grid = true):
	mouse_position_divisor = tile_map.cell_size if snap_to_grid else Vector2(1, 1)
	__show_selection_indicator(size)


func __show_selection_indicator(size):
	$MapSelectionIndicator.visible = true
	$MapSelectionIndicator.set_size(size * mouse_position_divisor)
	position_map_selection_indicator()


func hide_map_selection_indicator():
	$MapSelectionIndicator.visible = false


func position_map_selection_indicator():
	$MapSelectionIndicator.set_position(calc_indicator_position())


func calc_indicator_position():
	var mouse_pos = get_viewport().get_mouse_position()
	var x = floor(mouse_pos.x / mouse_position_divisor.x)
	var y = floor(mouse_pos.y / mouse_position_divisor.y)
	return Vector2(x, y)*mouse_position_divisor
