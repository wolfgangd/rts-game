extends Node2D

const Pulser = preload("res://game/effects/util/Pulser.gd")

var pulser

func _ready():
	pulser = Pulser.new(self, $ColorRect.rect_size, 0.5, 1.5)
	pulser.connect("finished", self, "_on_pulser_finished")
	pulser.pulse()

func _on_pulser_finished():
	queue_free()
