extends Node

var obj
var size
var anim_time
var scale_up
var orig_position
var orig_scale

signal finished

func _init(obj_, size_, anim_time_, scale_up_):
	obj = obj_
	size = size_
	anim_time = anim_time_
	scale_up = scale_up_
	orig_position = obj.position
	orig_scale = obj.scale
	


func pulse():
	obj.position = orig_position
	obj.scale = orig_scale
	var tween = obj.get_tree().create_tween()
	tween.connect("finished", self, "_on_tween_finished")
	tween.parallel().tween_property(obj, "scale", Vector2(1 + scale_up, 1 + scale_up), anim_time)
	tween.parallel().tween_property(obj, "position", orig_position - Vector2(size.x*scale_up/2, size.y*scale_up/2), anim_time)
	tween.tween_property(obj, "scale", orig_scale, anim_time)
	tween.parallel().tween_property(obj, "position", orig_position, anim_time)

func _on_tween_finished():
	queue_free()
	emit_signal("finished")
