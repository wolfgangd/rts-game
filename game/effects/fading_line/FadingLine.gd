extends Node2D


func _ready():
	$Line2D.width = 5



func init(start, end, color):
	$Line2D.add_point(start)
	$Line2D.add_point(end)
	$Line2D.default_color = color

	var tween = get_tree().create_tween()
	tween.connect("finished", self, "_on_tween_finished")
	var oc = color
	tween.tween_property($Line2D, "default_color", Color(oc.r, oc.g, oc.b, 0.0), 1);

func _on_tween_finished():
	queue_free()

	
