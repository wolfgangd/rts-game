extends Node2D

var pulsing_rect_effect = load("res://game/effects/pulsing_rect/PulsingRect.tscn")
var fading_line_effect = load("res://game/effects/fading_line/FadingLine.tscn")

func _ready():
	var effect1 = pulsing_rect_effect.instance()
	add_child(effect1)

	var effect2 = fading_line_effect.instance()
	add_child(effect2)
	effect2.init(Vector2(10, 10), Vector2(50, 100), Color.yellow)

