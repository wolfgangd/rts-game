extends Node

const Level1 = preload("res://game/Level1.gd")
const Level2 = preload("res://game/Level2.gd")
const Level3 = preload("res://game/Level3.gd")
var game_scene = load("res://game/Game.tscn")

var game
var levels = [Level1.level_data, Level2.level_data, Level3.level_data]
var current_level = 0

func _ready():
	Log.toggleGame()

func _on_new_game_requested():
	$MainMenu.visible = false
	game = game_scene.instance()
	add_child(game)
	game.connect("level_lost", self, "_on_level_lost")
	game.connect("level_won", self, "_on_level_won")
	game.start(levels[current_level])

func _on_exit_program_requested():
	get_tree().quit()

func _on_level_lost():
	self.__remove_game()
	$MainMenu.activate("YOU LOST!", "New Game")
	current_level = 0

func _on_level_won():
	self.__remove_game()
	$MainMenu.activate("YOU WON!", "Next Level")
	current_level = (current_level + 1)%levels.size()

func __remove_game():
	remove_child(game)
	game.queue_free()
