extends GutTest

const NavGenerator = preload("res://game/navigation/NavGenerator.gd")

var tilemap
var tile_set
var navpoly


func before_each():
	tilemap = double(TileMap).new()
	tile_set = double(TileSet).new()
	navpoly = double(NavigationPolygon).new()
	tilemap.tile_set = tile_set
	tilemap.tile_set = tile_set
	tilemap.cell_size = Vector2(64, 64)


func test_no_obstalecs():
	var nav_gen = autofree(NavGenerator.new(Vector2(2.0, 2.0), Vector2(1.0, 1.0)))
	stub(tilemap, "get_cell").to_return(0)  # Each cell has id 0
	stub(tile_set, "get_tiles_ids").to_return([0])  # 0 is the only tile id
	stub(tile_set, "tile_get_shape_count").to_return(0)  # tile id 0 has no collision shape

	nav_gen.generate(navpoly, tilemap)

	assert_called(navpoly, "clear_outlines")
	assert_called(
		navpoly,
		"add_outline",
		[[Vector2(0, 0), Vector2(64 * 2, 0), Vector2(64 * 2, 64 * 2), Vector2(0, 64 * 2)]]
	)
	assert_called(navpoly, "make_polygons_from_outlines")


func test_multiple_obstacle_ids():
	var player_width = 2.0
	var player_height = 1.0
	var nav_gen = autofree(
		NavGenerator.new(Vector2(4.0, 4.0), Vector2(player_width, player_height))
	)

	stub(tile_set, "get_tiles_ids").to_return([0, 1, 2])
	stub(tile_set, "tile_get_shape_count").to_return(0).when_passed(0)  # Only tile id 0 has no collision shape
	stub(tile_set, "tile_get_shape_count").to_return(1).when_passed(1)
	stub(tile_set, "tile_get_shape_count").to_return(1).when_passed(2)

	stub(tilemap, "get_cell").to_return(0)
	# Tile id 1 is 2x2 obstacle, Tile id 2 is 1x1 obstacle:
	#	. . . .
	#	. . 1 1
	#	. . 1 1
	#	2 . . .

	stub(tilemap, "get_cell").to_return(1).when_passed(2, 1)
	stub(tilemap, "get_cell").to_return(1).when_passed(3, 1)
	stub(tilemap, "get_cell").to_return(1).when_passed(2, 2)
	stub(tilemap, "get_cell").to_return(1).when_passed(3, 2)
	stub(tilemap, "get_cell").to_return(2).when_passed(0, 3)

	nav_gen.generate(navpoly, tilemap)

	assert_called(navpoly, "clear_outlines")
	assert_called(
		navpoly,
		"add_outline",
		[[Vector2(0, 0), Vector2(64 * 4, 0), Vector2(64 * 4, 64 * 4), Vector2(0, 64 * 4)]]
	)
	assert_called(
		navpoly,
		"add_outline",
		[
			[
				Vector2(2 * 64 - player_width, 64 - player_height),
				Vector2(4 * 64, 64 - player_height),
				Vector2(4 * 64, 3 * 64),
				Vector2(2 * 64 - player_width, 3 * 64)
			]
		]
	)

	assert_called(
		navpoly,
		"add_outline",
		[
			[
				Vector2(-player_width, 3 * 64 - player_height),
				Vector2(64, 3 * 64 - player_height),
				Vector2(64, 4 * 64),
				Vector2(-player_width, 4 * 64)
			]
		]
	)

	assert_called(navpoly, "make_polygons_from_outlines")
