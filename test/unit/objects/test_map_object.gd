extends GutTest

var map_object_scene = load("res://game/objects/map_object/MapObject.tscn")

var tile_map
var map_object


func before_each():
	tile_map = double(TileMap).new()
	tile_map.cell_size = Vector2(32, 32)
	map_object = autofree(map_object_scene.instance())


func test_place_on_map_sets_cells_in_tilemap():
	var template = {"size": [3, 2], "coords": [3, 4], "tile_id": 7}
	map_object.place_on_map(template, tile_map)

	# First row
	assert_set_cell(3, 4, 7)
	assert_set_cell(4, 4, 7)
	assert_set_cell(5, 4, 7)
	# Second row
	assert_set_cell(3, 5, 7)
	assert_set_cell(4, 5, 7)
	assert_set_cell(5, 5, 7)


func test_place_on_map_accepts_optional_coords():
	var template = {"size": [2, 1], "coords": [3, 4], "tile_id": 7}
	map_object.place_on_map(template, tile_map, [32*5, 32*6])

	# First row
	assert_set_cell(5, 6, 7)
	assert_set_cell(6, 6, 7)


func test_place_on_map_calculates_rect():
	var template = {"size": [3, 2], "coords": [3, 4], "tile_id": 7}
	map_object.place_on_map(template, tile_map)

	assert_eq(Vector2(3 * 32, 4 * 32), map_object.rect.position)
	assert_eq(Vector2(6 * 32, 6 * 32), map_object.rect.end)


func test_place_on_map_sets_some_basic_props_from_template():
	var template = {
		"size": [3, 2],
		"coords": [3, 4],
		"tile_id": 7,
		"type": "some_type",
		"id": "some_id",
		"name": "some name"
	}
	map_object.place_on_map(template, tile_map)
	assert_eq("some name", map_object.object_name)


class Receiver:
	var called_with_params = {}
	var called_with_arg = null

	func on_clicked(arg):
		called_with_arg = arg


func test_on_clicked():
	var template = {"size": [3, 2], "coords": [3, 4], "tile_id": 7}
	map_object.place_on_map(template, tile_map)

	var receiver = autofree(Receiver.new())
	map_object.on_clicked(receiver, "on_clicked", "some arg")
	click_object()
	# Receiver is called with arg passed on_clicked"
	assert_eq("some arg", receiver.called_with_arg)


func click_object():
	var sender = InputSender.new(map_object.get_node("Clickable"))
	sender.mouse_left_button_down(Vector2(1, 1))


func assert_set_cell(x, y, tile_id):
	assert_called(tile_map, "set_cell", [x, y, tile_id, false, false, false, Vector2(0, 0)])
