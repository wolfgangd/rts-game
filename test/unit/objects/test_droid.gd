extends GutTest

const Droid = preload("res://game/objects/Droid.gd")

var droid


func before_each():
	droid = autofree(Droid.new())


func test_droid_init():
	var template = {"name": "Fred"}
	droid.init(template)
	assert_eq(droid.get_name(), "Fred")
