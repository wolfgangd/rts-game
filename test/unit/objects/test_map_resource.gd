extends GutTest

var resource_object_scene = load("res://game/objects/map_resource/MapResource.tscn")

var resource
var info


func before_each():
	resource = autofree(resource_object_scene.instance())
	resource.get_node("MapObject").rect = Rect2(Vector2(0, 0), Vector2(64, 32))
	info = resource.get_node("MapObject").get_node("Info")


func test_init_sets_size_of_info():
	resource.init({"count": 15, "id": "foo"})
	assert_eq(Vector2(64, 32), info.get_size())
	assert_eq("15", info.text)


func test_take_until_empty():
	resource.init({"count": 15, "id": "foo"})
	assert_eq(10, resource.take(10))
	assert_eq(5, resource.take(10))
	assert_eq(0, resource.take(10))


func test_take_changes_info_text():
	resource.init({"count": 15, "id": "foo"})
	resource.take(6)
	assert_eq("9", info.text)
