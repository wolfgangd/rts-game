extends GutTest

var printer_object_scene = load("res://game/objects/map_printer/MapPrinter.tscn")
var storage_object_scene = load(
	"res://game/objects/map_resource_storage/MapResourceStorage.tscn"
)

const ObjectFactory = preload("res://game/objects/ObjectFactory.gd")
const LevelTemplate = preload("res://game/level/LevelTemplate.gd")

var printer
var storage

var droid_template = {
	"type": "engineering_droid",
	"id": "engineering_droid_1"
}

var teleporter_part_template = {
	"type": "teleporter_part",
	"id": "teleporter_part_1",
	"color": [0.5, 0, 0]
}


var level_data = {
	"world_object_templates": [droid_template, teleporter_part_template]
}

var printer_template = {
	"size": [3, 2],
	"coords": [3, 4],
	"tile_id": 7,
	"output":
	{
		"engineering_droid":
		{"requirements": [{"type": "resource", "id": "resource_1", "count": 14}]},
		"teleporter_part":
		{"requirements": [{"type": "resource", "id": "resource_1", "count": 1}]},

	}
}

var object_factory


func before_each():
	printer = autofree(printer_object_scene.instance())
	storage = autofree(storage_object_scene.instance())
	object_factory = double(ObjectFactory).new()
	printer.init(printer_template, storage)
	printer.setup(LevelTemplate.new(level_data), object_factory)
	var tile_map = double(TileMap).new()
	printer.place_on_map(printer_template, tile_map)



func test_printer_on_command_creates_requested_object():
	# Not enough resources in storage, so droid not created
	printer._on_command({"id": "print", "arg": "engineering_droid"})
	assert_not_called(object_factory, "create")


	# Shut up warning about stub returning null
	stub(object_factory, "create").to_return({})
	
	# Enough resources in storage, so droid created with correct template
	storage.put(100, "resource_1")
	printer._on_command({"id": "print", "arg": "engineering_droid"})
	assert_called(object_factory, "create", [droid_template, printer.get_rect().end])

	# Resource was taken from storage
	assert_eq(storage.check("resource_1"), 100 - 14)

func test_printer_teleporter_part_can_be_taken_once():
	storage.put(100, "resource_1")

	printer._on_command({"id": "print", "arg": "teleporter_part"})

	assert_eq_deep(printer.take_part("teleporter_part"), {})
	assert_null(printer.take_part("teleporter_part"))
