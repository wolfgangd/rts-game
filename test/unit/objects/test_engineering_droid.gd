extends GutTest

var droid_scene = load("res://game/objects/engineering_droid/EngineeringDroid.tscn")
var printer_scene = load("res://game/objects/map_printer/MapPrinter.tscn")
var mover_scene = load("res://game/navigation/Mover.tscn")

const LevelTemplate = preload("res://game/level/LevelTemplate.gd")
const ObjectFactory = preload("res://game/objects/ObjectFactory.gd")

var droid
var level_template
var object_factory
var printer
var mover

var printer_pos = Vector2(1, 2)
var build_pos = Vector2(3, 4)
var turret_droid_template = {"type": "turrent_droid", "parts": ["turret_droid_part"]}


func before_each():
	droid = autofree(droid_scene.instance())
	droid.pickup_time = 0.1
	watch_signals(droid)
	add_child(droid)

	level_template = double(LevelTemplate).new()
	object_factory = double(ObjectFactory).new()
	printer = double(printer_scene).instance()
	mover = double(mover_scene).instance()

	droid.set_level_template(level_template)
	droid.set_object_factory(object_factory)

	stub(level_template, "get_first_template_with_type").to_return(turret_droid_template).when_passed(
		"turret_droid"
	)
	stub(object_factory, "create")
	stub(printer, "get_position").to_return(printer_pos)
	stub(mover, "navigate_to")

	replace_node(droid, "Mover", mover)
	# _process can not be stubbed, this shuts up warnings
	mover.set_process(false)


func test_eng_droid_build_turret_droid():
	stub(printer, "take_part").to_return({}).when_passed("turret_droid_part")

	droid._on_command({"id": "build-turret-droid"})

	assert_signal_emitted_with_parameters(droid, "placing_required", [turret_droid_template])

	droid.goto_printer(printer, build_pos)
	assert_called(mover, "navigate_to", [printer_pos])

	droid._on_destination_reached()

	yield(yield_for(0.15), YIELD)
	assert_called(mover, "navigate_to", [build_pos - Vector2(16, 16)])
	droid._on_destination_reached()
	assert_called(
		object_factory, "create", [turret_droid_template, [build_pos.x, build_pos.y]]
	)


func test_eng_droid_stops_because_no_parts_in_printer():
	stub(printer, "take_part").to_return(null)

	droid._on_command({"id": "build-turret-droid"})
	droid.goto_printer(printer, build_pos)
	droid._on_destination_reached()

	yield(yield_for(0.15), YIELD)
	assert_not_called(object_factory, "droid")
