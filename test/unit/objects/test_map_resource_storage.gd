extends GutTest

var scene = load("res://game/objects/map_resource_storage/MapResourceStorage.tscn")

var storage


func before_each():
	storage = autofree(scene.instance())


func test_put_adds_amount():
	assert_eq(storage.check("resource_1"), 0)
	assert_eq(storage.check("resource_2"), 0)

	storage.put(10, "resource_1")
	storage.put(1, "resource_1")
	storage.put(20, "resource_2")
	storage.put(2, "resource_2")

	assert_eq(storage.check("resource_1"), 11)
	assert_eq(storage.check("resource_2"), 22)


func test_take_removes_amount():
	storage.put(10, "resource_1")
	storage.put(20, "resource_2")
	storage.take(3, "resource_1")
	storage.take(17, "resource_2")
	assert_eq(storage.check("resource_1"), 7)
	assert_eq(storage.check("resource_2"), 3)


func test_take_can_be_all_or_nothing():
	storage.put(10, "resource_1")
	storage.put(20, "resource_2")

	assert_true(storage.take(3, "resource_1"))
	assert_true(storage.take(17, "resource_2"))

	assert_false(storage.take(10, "resource_1"))
	assert_false(storage.take(20, "resource_2"))

	assert_eq(storage.check("resource_1"), 7)
	assert_eq(storage.check("resource_2"), 3)


func test_emits_signal_on_put():
	watch_signals(storage)
	storage.put(15, "resource_1")
	assert_signal_emitted_with_parameters(storage, "resource_storage_changed", [15, "resource_1"])
	storage.put(5, "resource_3")
	assert_signal_emitted_with_parameters(storage, "resource_storage_changed", [5, "resource_3"])
