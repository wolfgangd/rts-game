extends GutTest

const ObjectFactory = preload("res://game/objects/ObjectFactory.gd")

const GatheringDroid = preload("res://game/objects/gathering_droid/GatheringDroid.gd")
const EngineeringDroid = preload("res://game/objects/engineering_droid/EngineeringDroid.gd")
const EnemyDroid = preload("res://game/objects/enemy_droid/EnemyDroid.gd")
const TurretDroid = preload("res://game/objects/turret_droid/TurretDroid.gd")

const MapResource = preload("res://game/objects/map_resource/MapResource.gd")
const MapResourceStorage = preload("res://game/objects/map_resource_storage/MapResourceStorage.gd")
const MapPrinter = preload("res://game/objects/map_printer/MapPrinter.gd")
const MapTeleporter = preload("res://game/objects/map_teleporter/MapTeleporter.gd")
const BuildingMaterial = preload("res://game/objects/building_material/BuildingMaterial.gd")

var resource_storage_scene = load(
	"res://game/objects/map_resource_storage/MapResourceStorage.tscn"
)

var factory
var tile_map


func before_each():
	tile_map = double(TileMap).new()
	factory = ObjectFactory.new(tile_map)
	watch_signals(factory)


func test_create_gathering_droid_by_template():
	var template = {"id": "droid_1", "type": "gathering_droid", "name": "Fred", "color": [1, 1, 1]}

	var droid = factory.create(template, [1, 2])
	assert_true(droid is GatheringDroid)
	assert_eq(Vector2(1, 2), droid.get_position())
	assert_eq("gathering_droid", droid.get_type())
	assert_eq("Fred", droid.get_name())
	droid.free()


func test_create_engineering_droid_by_template():
	var template = {"id": "droid_1", "type": "engineering_droid", "name": "Bob", "color": [1, 1, 1]}

	var droid = factory.create(template, [1, 2])
	assert_is(droid, EngineeringDroid)
	assert_eq(Vector2(1, 2), droid.get_position())
	assert_eq("engineering_droid", droid.get_type())
	assert_eq("Bob", droid.get_name())
	droid.free()
	

func test_create_droid_get_position_from_template_if_not_given():
	var template = {
		"id": "droid_1",
		"type": "engineering_droid",
		"name": "Bob",
		"color": [1, 1, 1],
		"position": [10, 20]
	}

	var droid = factory.create(template)
	assert_eq(Vector2(10, 20), droid.get_position())
	droid.free()


func test_create_enemy_droid_by_template():
	var template = {"id": "droid_1", "type": "enemy_droid", "name": "Hank"}
	var droid = factory.create(template, [1, 2])
	assert_is(droid, EnemyDroid)

	droid.free()


func test_create_turret_droid_by_template():
	var template = {"id": "droid_1", "type": "turret_droid", "name": "Hank", "position": [1, 2]}
	var droid = factory.create(template)
	assert_is(droid, TurretDroid)

	droid.free()


func test_create_droid_emits_signal():
	var template = {"id": "droid_1", "type": "engineering_droid", "name": "Bob", "color": [1, 1, 1]}

	var droid = factory.create(template, [1, 2])

	assert_signal_emitted_with_parameters(factory, "droid_created", [droid])

	droid.free()


func test_create_droid_generates_id():
	var template = {"type": "turret_droid", "name": "Hank", "id": "turret_droid_template_1"}
	var droid_1 = factory.create(template, [1, 2])
	var droid_2 = factory.create(template, [3, 4])
	assert_eq(droid_1.get_id(), "turret_droid_1")
	assert_eq(droid_2.get_id(), "turret_droid_2")

	droid_1.free()
	droid_2.free()


func test_create_resource_by_template():
	var template = {
		"type": "resource",
		"id": "resource_27",
		"count": 100,
		"size": [1, 1],
		"coords": [3, 4],
		"tile_id": 12
	}

	var resource = factory.create(template)
	assert_is(resource, MapResource)
	assert_eq(resource.get_type(), "resource")
	assert_eq(resource.get_id(), "resource_1")
	assert_eq("resource_27", resource.resource_id)
	assert_eq(100, resource.count)
	assert_signal_emitted_with_parameters(factory, "resource_created", [resource])
	resource.free()


func test_create_resource_storage_by_template():
	var template = {"type": "resource_storage", "size": [1, 1], "coords": [3, 4], "tile_id": 12}

	var storage = factory.create(template)
	assert_is(storage, MapResourceStorage)
	assert_eq(storage.get_type(), "resource_storage")
	assert_eq(storage.get_id(), "resource_storage_1")

	assert_signal_emitted_with_parameters(factory, "resource_storage_created", [storage])
	storage.free()


func test_create_printer_by_template():
	var template = {
		"type": "printer", "size": [1, 1], "coords": [3, 4], "tile_id": 12, "output": []
	}
	var resource_storage = double(resource_storage_scene).instance()

	var printer = factory.create(template, resource_storage)
	assert_is(printer, MapPrinter)
	assert_eq(printer.get_type(), "printer")
	assert_eq(printer.get_id(), "printer_1")

	assert_signal_emitted_with_parameters(factory, "printer_created", [printer])
	printer.free()


func test_create_teleporter_by_template():
	var template = {"type": "teleporter", "size": [1, 1], "tile_id": 12, "build_time": 30}
	var teleporter = factory.create(template, [3, 4])
	assert_is(teleporter, MapTeleporter)
	assert_eq(teleporter.get_type(), "teleporter")
	assert_eq(teleporter.get_id(), "teleporter_1")

	assert_signal_emitted_with_parameters(factory, "teleporter_created", [teleporter])
	teleporter.free()
