extends GutTest

var level_data = {
	"world_object_templates":
	[
		{
			"type": "resource",
			"id": "resource_1",
		},
		{
			"type": "resource",
			"id": "resource_2",
		},
		{
			"type": "resource_storage",
			"id": "resource_storage_1",
		},
		{
			"type": "gathering_droid",
			"id": "gathering_droid_1",
		},
		{
			"type": "engineering_droid",
			"id": "engineering_droid_1",
		},
		{"type": "turret_droid"}
	]
}

const LevelTemplate = preload("res://game/level/LevelTemplate.gd")

var level_template


func before_each():
	level_template = LevelTemplate.new(level_data)


func test_get_template_by_id():
	assert_eq_deep(
		{"type": "resource", "id": "resource_1"}, level_template.get_template_by_id("resource_1")
	)

	assert_eq_deep(
		{"type": "resource", "id": "resource_2"}, level_template.get_template_by_id("resource_2")
	)

	assert_eq_deep(
		{"type": "engineering_droid", "id": "engineering_droid_1"},
		level_template.get_template_by_id("engineering_droid_1")
	)


func test_get_template_by_id_return_null_if_not_found():
	assert_null(level_template.get_template_by_id("foo"))


func test_get_first_template_with_type():
	assert_eq_deep(
		{"type": "resource", "id": "resource_1"},
		level_template.get_first_template_with_type("resource")
	)
	assert_null(level_template.get_first_template_with_type("foo"))


func test_get_all_templates():
	assert_eq_deep(level_data["world_object_templates"], level_template.get_all_templates())
