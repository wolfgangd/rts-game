extends GutTest

const LevelObjects = preload("res://game/level/LevelObjects.gd")
const ObjectBase = preload("res://game/objects/ObjectBase.gd")

var level_objects


func before_each():
	level_objects = LevelObjects.new()


func test_register_map_objects_with_different_types():
	var object_1 = __make_object("type_1", "object_1")
	var object_2 = __make_object("type_1", "object_2")
	var object_3 = __make_object("type_2", "object_3")

	level_objects.register_object(object_1)
	level_objects.register_object(object_2)
	level_objects.register_object(object_3)
	assert_eq(level_objects.get_object("type_1", "object_1"), object_1)
	assert_eq(level_objects.get_object("type_1", "object_2"), object_2)
	assert_eq(level_objects.get_object("type_2", "object_3"), object_3)


func test_get_object_returns_null_if_none_found():
	var object_1 = __make_object("type_1", "object_1")
	level_objects.register_object(object_1)

	assert_null(level_objects.get_object("type_1", "some_id"))
	assert_null(level_objects.get_object("some_type", "some_id"))


func test_get_objects_with_type():
	var object_1 = __make_object("type_1", "object_1")
	var object_2 = __make_object("type_1", "object_2")
	var object_3 = __make_object("type_2", "object_3")

	level_objects.register_object(object_1)
	level_objects.register_object(object_2)
	level_objects.register_object(object_3)
	assert_eq(level_objects.get_objects_with_type("type_1"), [object_1, object_2])


func test_get_objects_with_type_empty_array_if_type_not_registerd():
	assert_eq(level_objects.get_objects_with_type("type_1"), [])


func test_register_droids_with_different_types():
	var object_1 = __make_object("type_1", "object_1")
	var object_2 = __make_object("type_1", "object_2")
	var object_3 = __make_object("type_2", "object_3")

	level_objects.register_object(object_1)
	level_objects.register_object(object_2)
	level_objects.register_object(object_3)
	assert_eq(level_objects.get_object("type_1", "object_1"), object_1)
	assert_eq(level_objects.get_object("type_1", "object_2"), object_2)
	assert_eq(level_objects.get_object("type_2", "object_3"), object_3)


func test_destroy_object_removes_it():
	var object_1 = __make_object("type_1", "object_1")
	var object_2 = __make_object("type_1", "object_2")
	var object_3 = __make_object("type_2", "object_3")

	level_objects.register_object(object_1)
	level_objects.register_object(object_2)
	level_objects.register_object(object_3)

	level_objects.destroy_object(object_1)
	assert_null(level_objects.get_object("type_1", "object_1"))
	assert_not_null(level_objects.get_object("type_1", "object_2"))
	assert_not_null(level_objects.get_object("type_2", "object_3"))

	level_objects.destroy_object(object_2)
	assert_null(level_objects.get_object("type_1", "object_1"))
	assert_null(level_objects.get_object("type_1", "object_2"))
	assert_not_null(level_objects.get_object("type_2", "object_3"))

	level_objects.destroy_object(object_3)
	assert_null(level_objects.get_object("type_1", "object_1"))
	assert_null(level_objects.get_object("type_1", "object_2"))
	assert_null(level_objects.get_object("type_2", "object_3"))


func test_destroy_object_does_nothing_if_not_exist():
	var object_1 = __make_object("type_1", "object_1")
	# No object of that type
	level_objects.destroy_object(object_1)
	var object_2 = __make_object("type_1", "object_2")
	level_objects.register_object(object_1)
	# Other object of a registered type
	level_objects.destroy_object(object_2)
	assert_true(true)


func test_destroy_object_emits_signal():
	watch_signals(level_objects)

	var object_1 = __make_object("type_1", "object_1")
	level_objects.register_object(object_1)
	level_objects.destroy_object(object_1)
	assert_signal_emitted_with_parameters(level_objects, "object_destroyed", [object_1])


func __make_object(type, id):
	var object = double(ObjectBase).new()
	stub(object, "get_type").to_return(type)
	stub(object, "get_id").to_return(id)
	return object
