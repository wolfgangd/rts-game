#!/bin/bash

args=""
if [ "$1" != "" ]; then
    args="-gunit_test_name=$1"
fi

godot --no-window -d --path "$PWD" -s addons/gut/gut_cmdln.gd -gconfig=./.gut_editor_config.json $args 2>/dev/null
